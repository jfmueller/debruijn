{-# LANGUAGE RankNTypes #-}

-- | Implementation of well-formed lambda terms with a type-level encoding of 
--   DeBruijn indices, based on Richard S. Bird and Ross Patterson: "de 
--   Bruijn notation as a nested datatype", J. of Functional Programming
--   Vol. 9, Issue 1 1999, p. 77.91
module DeBruijn(
  Slot(..),
  Term(..),
  abstract,
  match,
  apply,
  subst
  ) where

data Slot a = Zero | Succ a
    deriving (Eq, Ord, Show)

instance Functor Slot where
    fmap _ Zero = Zero
    fmap f (Succ a) = Succ (f a)

data Term a
  = Ap (Term a) (Term a)
  | Lam (Term (Slot a))
  | Var a
  deriving (Eq, Ord, Show)

instance Functor Term where
    fmap f e = case e of
        Ap l r -> Ap (fmap f l) (fmap f r)
        Lam e'  -> (Lam . fmap (fmap f)) e'
        Var a  -> (Var . f) a

foldT :: (forall a. a -> n a) ->
         (forall a. n a -> n a -> n a) ->
         (forall a. n (Slot a) -> n a) ->
         Term b -> 
         n b
foldT v a l t = case t of
    Var x -> v x
    Ap l' r -> let q = foldT v a l in
               a (q l') (q r)
    Lam t' -> (l . foldT v a l) t'

gfoldT :: (forall a. m a -> n a) ->
          (forall a. n a -> n a -> n a) ->
          (forall a. n (Slot a) -> n a) ->
          (forall a. Slot (m a) -> m (Slot a)) ->
          Term (m b) ->
          n b
gfoldT v a l k t = let q = gfoldT v a l k in
                   case t of
                    Var x   -> v x
                    Ap l' r -> a (q l') (q r)
                    Lam t'  -> (l . q . fmap k) t'

joinT :: Term (Term a) -> Term a
joinT = gfoldT id Ap Lam distT

distT :: Slot (Term a) -> Term (Slot a)
distT s = case s of
    Zero   -> Var Zero
    Succ a -> fmap Succ a

instance Monad Term where 
    return = Var
    e >>= f = joinT $ fmap f e

abstract :: Eq a => a -> Term a -> Term a
abstract x = Lam . fmap (match x)

match :: Eq a => a -> a -> Slot a
match x y = if x == y then Zero else Succ y

apply :: Term a -> Term (Slot a) -> Term a
apply t = joinT . fmap (subst t . fmap Var)

subst :: a -> Slot a -> a
subst a s = case s of
    Zero -> a
    Succ y -> y
